<?php

namespace Drupal\field_delta_display_block\Service;

use Drupal\Core\Entity\EntityInterface;

/**
 * A service for getting the values from a field.
 */
class FieldValueLoader {

  /**
   * The entity to load a field from.
   *
   * @var \Drupal\Core\Entity\EntityInterface|null
   */
  private $entity = NULL;

  /**
   * The name of the field to load.
   *
   * @var string|null
   */
  private $fieldName = NULL;

  /**
   * The field delta to load.
   *
   * @var int
   */
  private $delta = 0;

  /**
   * Set the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return $this
   */
  public function setEntity(EntityInterface $entity) {
    $this->entity = $entity;

    return $this;
  }

  /**
   * Set the field name.
   *
   * @param string $fieldName
   *   The name of the field.
   *
   * @return $this
   */
  public function setFieldName($fieldName) {
    $this->fieldName = $fieldName;

    return $this;
  }

  /**
   * Set the field delta
   *
   * @param int $delta
   *   The field delta.
   *
   * @return $this
   */
  public function setDelta($delta) {
    $this->delta = $delta;

    return $this;
  }

  /**
   * Get the field value.
   *
   * @return mixed|null
   *   The field value.
   */
  public function getValue() {
    if (!$fieldData = $this->entity->get($this->fieldName)->get($this->delta)) {
      return NULL;
    }

    return $fieldData->getValue();
  }

}
