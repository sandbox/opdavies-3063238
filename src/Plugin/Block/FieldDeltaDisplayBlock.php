<?php

namespace Drupal\field_delta_display_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\field_delta_display_block\Service\FieldValueLoader;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a 'FieldDeltaDisplay' block.
 *
 * @Block(
 *  id = "field_delta_display",
 *  admin_label = @Translation("Field Delta Display block"),
 * )
 */
class FieldDeltaDisplayBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * The field value loader service.
   *
   * @var \Drupal\field_delta_display_block\Service\FieldValueLoader
   */
  private $fieldValueLoader;

  /**
   * {@inheritDoc}
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    RequestStack $requestStack,
    FieldValueLoader $fieldValueLoader
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);

    $this->requestStack = $requestStack;
    $this->fieldValueLoader = $fieldValueLoader;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('request_stack'),
      $container->get(FieldValueLoader::class)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $formState) {
    $form['field_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Field name'),
      '#default_value' => $this->configuration['field_name'],
      '#maxlength' => 64,
      '#size' => 1,
      '#options' => [
        'body' => 'Body',
      ],
      '#required' => TRUE,
    ];
    $form['delta'] = [
      '#type' => 'number',
      '#title' => $this->t('Instance (delta)'),
      '#description' => $this->t('Select which instance of the field to display. Starts at 1 (delta 0).'),
      '#default_value' => !empty($this->configuration['delta']) ? $this->configuration['delta'] : 1,
      '#maxlength' => 64,
      '#size' => 64,
      '#min' => 1,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $formState) {
    $this->configuration['field_name'] = $formState->getValue('field_name');
    $this->configuration['delta'] = $formState->getValue('delta') - 1;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (!$node = \Drupal::requestStack()->getCurrentRequest()->get('node')) {
      return [];
    }

    $fieldName = $this->configuration['field_name'];
    $delta = $this->configuration['delta'];

    $fieldData = $this->fieldValueLoader
      ->setEntity($node)
      ->setFieldName($fieldName)
      ->setDelta($delta)
      ->getValue();

    if (!$fieldData) {
      return [];
    }

    $build = [];
    $build['#type'] = 'processed_text';
    $build['#format'] = $fieldData['format'];
    $build['#text'] = $fieldData['value'];

    return $build;
  }

}
