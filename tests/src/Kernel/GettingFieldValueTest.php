<?php

namespace Drupal\Tests\field_delta_display_block\Kernel;

use Drupal\field_delta_display_block\Service\FieldValueLoader;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Test getting values from a multi-value field.
 */
class GettingFieldValueTest extends EntityKernelTestBase {

  use NodeCreationTrait;

  const BODY_FIELD_VALUE_ONE = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
  const BODY_FIELD_VALUE_TWO = 'Vestibulum et massa vel felis rhoncus pulvinar id vitae sapien.';
  const BODY_FIELD_VALUE_THREE = 'Vestibulum sed fermentum felis.';

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'filter',
    'node',
    'field_delta_display_block',
    'field_delta_display_block_test',
  ];

  /**
   * The field value loader service.
   *
   * @var \Drupal\field_delta_display_block\Service\FieldValueLoader
   */
  private $fieldValueLoader;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installConfig(self::$modules);

    $this->fieldValueLoader = $this->container->get(FieldValueLoader::class);
  }

  /**
   * Test getting the value of a multi-value field.
   *
   * @dataProvider bodyValues
   */
  public function testGetFieldValue($delta, $fieldValue) {
    $node = $this->createNode([
      'type' => 'page',
      'body' => [
        self::BODY_FIELD_VALUE_ONE,
        self::BODY_FIELD_VALUE_TWO,
        self::BODY_FIELD_VALUE_THREE,
      ],
    ]);

    $this->assertCount(3, $node->get('body')->getValue());

    $this->fieldValueLoader->setEntity($node);
    $this->fieldValueLoader->setFieldName('body');

    $this->assertSame(
      ['value' => $fieldValue, 'summary' => NULL],
      $this->fieldValueLoader->setDelta($delta)->getValue()
    );
  }

  /**
   * Provides body values for testing.
   *
   * @return array
   */
  public function bodyValues() {
    return [
      [0, self::BODY_FIELD_VALUE_ONE],
      [1, self::BODY_FIELD_VALUE_TWO],
      [2, self::BODY_FIELD_VALUE_THREE],
    ];
  }

}
